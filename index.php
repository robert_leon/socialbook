<!DOCTYPE html>
<html>

<head>
    <link rel="icon" type="images/jpg" href="images/logo-mic.jpg">
    <meta charset="UTF-8">
    <link href="css/shift.css" rel="stylesheet">

    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">

</head>

<body>
<?php include("webComponents/headerBar.php"); ?>

<div class="jumbotron">
    <div class="container">
        <h1>Find a book to read.</h1>
        <p>Borrow books from people in 4 cities.</p>
        <a href="help.php">Learn More</a>
    </div>
</div>

<div class="neighborhood-guides">
    <div class="container">
        <h2>Neighborhood Guides</h2>
        <p>Not sure where you find us? We've created neighbourhood guides for cities all around Romania.</p>
        <div class="row">
            <div class="col-md-4">
                <div class="thumbnail">
                    <h3>Timisoara</h3>
                    <img src="images/timisoara.jpg">
                </div>
                <div class="thumbnail">
                    <h3>Brasov</h3>
                    <img src="images/brasov.jpg">
                </div>
            </div>

            <div class="col-md-4">
                <div class="thumbnail">
                    <h3>Bucuresti</h3>
                    <img src="images/bucuresti.jpg">
                </div>
                <div class="thumbnail">
                    <h3>Iasi</h3>
                    <img src="images/iasi.jpg">
                </div>
            </div>
        </div>
    </div>

    <div class="learn-more">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3>Find interesting books</h3>
                    <p>From poety and short stories to novels: find books based on your personal taste.</p>
                </div>
                <div class="col-md-4">
                    <h3>Borrow books</h3>
                    <p>Borrow your books and help the cultural community.</p>
                </div>
                <div class="col-md-4">
                    <h3>Trust and Safety</h3>
                    <p>Make book exchanges where do you want by calling the owner.</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>