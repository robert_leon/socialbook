/**
 * Created by Robert on 1/5/2016.
 */
function signUp() {
    var notify = "";
    var x = document.forms["signUpForm"]["firstname"].value;
    if (x == null || x == "") {
        notify += "First name must be filled out\n"
    }

    var y = document.forms["signUpForm"]["lastname"].value;
    if (y == null || y == "") {
        notify += "Last name must be filled out\n"
    }

    var z = document.forms["signUpForm"]["phone"].value;
    if (z == null || z == "" || isNaN(z)!=false || z.length!=10)
    {
        notify += "Phone must be filled out/Field is invalid\n"
    }

    var m=document.forms["signUpForm"]["email"].value;
    var atPos = m.indexOf("@");
    var dotPos = m.lastIndexOf(".");
    if (atPos < 1 || dotPos < atPos || dotPos > m.length)
    {
        notify += "Not a valid e-mail address\n"
    }

    var n=document.forms["signUpForm"]["password1"].value;
    if (n == null || n == "")
    {
        notify += "Password must be filled out\n"
    }

    var p=document.forms["signUpForm"]["password2"].value;
    if (p == null || p == "")
    {
        notify += "Confirm password must be filled out\n"
    }
    if (n != p)
    {
        notify += "Password doesn't match\n"
    }

    var c=document.forms["signUpForm"]["city"].value;
    if (c == null || c == "")
    {
        notify += "City must be filled out\n"
    }

    var o=document.forms["signUpForm"]["country"].value;
    if (o == null || o == "")
    {
        notify += "Country must be filled out\n"
    }



    if (notify != "")
    {
        alert(notify);
        return false;
    }
    else
    {
        signUpForm.Submit();
    }
}