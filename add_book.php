<html>
<head>
    <link rel="icon" type = "images/jpg" href = "images/logo-mic.jpg">
    <meta charset="UTF-8">
    <link href="css/shift.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <head>
        <?php include("webComponents/loggedBar.php") ?>
        <div class="row">
            <div class="col-sm-3">
                <div class="sidebar-nav">
                    <div class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <span class="visible-xs navbar-brand">Sidebar menu</span>
                        </div>
                        <div class="navbar-collapse collapse sidebar-navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="my_books.php">My books <span class="badge">0</span></a></li>
                                <li><a href="my_requests.php">My requests <span class="badge">0</span></a></li>
                                <li class="active"><a href="add_book.php">Add Book</a></li>
                                <li><a href="search.php">Search books</a></li>

                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <?php
                    include("services/add_book_logic.php");
                ?>
                </br>
                <div class="container">
                    <h3 ><i>Add book</i></h3>
                    <table class="not">
                        <form method="POST" name="addForm" action="add_book.php" onSubmit="return addBook();">
                            <tr>
                                <td height="40">Title:</td>
                                <td><input name="title" type="text" id="title" size="40"/></td>
                            </tr>
                            <tr>
                                <td height="40">Author:</td>
                                <td><input name="author" type="text" id="author" size="40"/></td>
                            </tr>
                            <tr>
                                <td height="40">Genres:</td>
                                <td><select name="genres">
                                    <option value="Story">Story</option>
                                    <option value="Drama">Drama</option>
                                    <option value="Novel">Novel</option>
                                    <option value="Poetry">Poetry</option>
                                    <option value="Fiction">Fiction</option>
                                    <option value="Biography">Biography</option>
                                    <option value="History">History</option>
                                    <option value="Science">Science</option>
                                    <option value="Religious">Religious</option>
                                    <option value="Philosophy">Philosophy</option>
                                    <option value="Comic">Comic</option>
                                    <option value="Food">Food</option>
                                    <option value="Hobby">Hobby</option>
                                    </select></td>
                               <!-- <td><input name="genres" type="text" id="genres" size="40"/></td>-->
                            </tr>
                            <tr>
                                <td align="right" colspan="2"><input type="submit" name="Submit" value="Submit"/>
                                    <input type="reset" name="reset" value="Reset"/></td>
                            </tr>
                        </form>
                    </table>
                    <br><br>
                </div>
            </div>
        </div>
</html>