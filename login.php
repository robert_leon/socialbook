<html>
<head>
    <link rel="icon" type="images/jpg" href="images/logo-mic.jpg">
    <meta charset="UTF-8">
    <title>Login</title>
    <link href="css/shift.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<?php include("webComponents/headerBar.php"); ?>

<div>
    <form id="loginForm" action="" method="POST">
    <h2><i>Sign in to your account</i></h2>
        <br>
        <table align="center">
            <tr>
                <?php
                include("services/login_logic.php");
                if (isset($error)) { ?>
                <small style="color:#aa0000;"><?php echo $error; ?>
                    <br/> <br/>
                    <?php } ?>
            </tr>
            <tr>
                <td width="113">Email:</td>
                <td width="215">
                    <label>
                        <input name="email" type="text" size="40"/>
                    </label>
                </td>
            </tr>
            <tr>
                <td>Password:</td>
                <td>
                    <label>
                        <input name="password" type="password" size="40"/>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" name="submit" value="Login"/>
                </td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>