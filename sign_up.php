<!DOCTYPE html>
<html>
<head>
    <link rel="icon" type="images/jpg" href="images/logo-mic.jpg">
    <meta charset="UTF-8">
    <title>Sign Up</title>
    <link href="css/shift.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <script type="text/javascript" src="js/sign_up.js"></script>
</head>
<body>
<?php include("webComponents/headerBar.php"); ?>

<div class="MainPage">
    <?php
    include("services/sign_up_logic.php");
    ?>

    <div class="container">
        <h3 ><i>Create An Account</i></h3>
        <table>
            <form method="POST" name="signUpForm" action="sign_up.php" onSubmit="return signUp();">
                <tr>
                    <td height="40">First Name:</td>
                    <td><input name="firstname" type="text" id="firstname" size="40"/></td>
                </tr>
                <tr>
                    <td height="40">Last Name:</td>
                    <td><input name="lastname" type="text" id="lastname" size="40"/></td>
                </tr>
                <tr>
                    <td height="40">Phone:</td>
                    <td><input name="phone" type="text" id="phone" size="40" class="phone"/></td>
                </tr>
                <tr>
                    <td height="40">E-mail:</td>
                    <td><input name="email" type="text" id="email" size="40"/></td>
                </tr>

                <tr>
                    <td height="40">Password:</td>
                    <td><input name="password1" type="password" id="password1" size="40"/></td>
                </tr>
                <tr>
                    <td height="40">Confirm Password:</td>
                    <td><input name="password2" type="password" id="password2" size="40"/></td>
                </tr>
                <br>
                <tr>
                    <td height="40">City</td>
                    <td><input name="city" type="text" id="city" size="40"/></td>
                </tr>
                <br>
                <tr>
                    <td height="40">Country</td>
                    <td><input name="country" type="text" id="country" size="40"/></td>
                </tr>
                <tr>
                    <td align="right" colspan="2"><input type="submit" name="Submit" value="Submit"/>
                        <input type="reset" name="reset" value="Reset"/></td>
                </tr>
            </form>
        </table>
        <br><br>
    </div>
</div>
</body>
</html>