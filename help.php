<!DOCTYPE html>
<html>
<head>
	<link rel="icon" type = "images/jpg" href = "images/logo-mic.jpg">
	<meta charset="UTF-8">
	<title>Sign Up</title>
	 <link href="css/shift.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<?php include("webComponents/headerBar.php"); ?>

	<div class="jumbotron-help">
      <div class="container">
        <h1>SocialBook concept.</h1><br><br>
        <p>You can borrow books from people logged in our system.</p>
      </div>
    </div> 
</body>
</html>