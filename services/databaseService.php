<?php
include("database_connect.php");
include_once("model/User.php");
include_once("model/Book.php");

function getUser($email)
{
    $user = new User();
    $connection = getConnection();
    $query = sprintf("SELECT * FROM user WHERE email='%s'", $email);
    $result = mysqli_query($connection, $query);
    $rows = mysqli_num_rows($result);

    if ($rows > 0) {
        $row = $result->fetch_assoc();
        $user->$email = $row['email'];
        $user->firstName = $row['first_name'];
        $user->lastName= $row['last_name'];
        $user->country= $row['country'];
        $user->city= $row['city'];
        $user->phone= $row['phone'];
        closeConnection($connection);
        return $user;
    }
    else
    {
        return null;
    }
}

function getUserBooks($email)
{
    $connection = getConnection();
    $books = array();
    $query = sprintf("SELECT * FROM book WHERE owner_id='%s'", $email);
    $result = mysqli_query($connection, $query);
    $rows = mysqli_num_rows($result);
    $owner = getUser($email);

    if ($rows > 0) {
        while($row = $result->fetch_assoc()) {
            $book = new Book();
            $book->bookId = $row["book_id"];
            $book->title = $row['title'];
            $book->author = $row['author'];
            $book->genres = $row['genres'];
            $book->available = $row['available'];
            $book->owner = $owner;
            array_push($books, $book);
        }
    }

    closeConnection($connection);
    return $books;
}

function searchBooks($user, $title)
{
    $connection = getConnection();
    $books = array();
    $query = sprintf("SELECT * FROM book WHERE title like '%%%s%%' and author!='%s'", $title, $user);
    //echo $query;
    $result = mysqli_query($connection, $query);
    $rows = mysqli_num_rows($result);

    if ($rows > 0) {
        while($row = $result->fetch_assoc()) {
            $book = new Book();
            $book->bookId = $row["book_id"];
            $book->title = $row['title'];
            $book->author = $row['author'];
            $book->genres = $row['genres'];
            $book->available = $row['available'];
            $ownerId = $row['owner_id'];
            $book->owner = getUser($ownerId);
            array_push($books, $book);
        }
    }

    closeConnection($connection);
    return $books;
}

function searchGBooks($user, $genres)
{
    $connection = getConnection();
    $books = array();
    $query = sprintf("SELECT * FROM book WHERE genres like '%%%s%%' and author!='%s'", $genres, $user);
    //echo $query;
    $result = mysqli_query($connection, $query);
    $rows = mysqli_num_rows($result);

    if ($rows > 0) {
        while($row = $result->fetch_assoc()) {
            $book = new Book();
            $book->bookId = $row["book_id"];
            $book->title = $row['title'];
            $book->author = $row['author'];
            $book->genres = $row['genres'];
            $book->available = $row['available'];
            $ownerId = $row['owner_id'];
            $book->owner = getUser($ownerId);
            array_push($books, $book);
        }
    }

    closeConnection($connection);
    return $books;
}

?>