<?php
//session_start();
//$error = ''; // Variable To Store Error Message

if (isset($_POST['submit'])) {
    if (empty($_POST['search'])) {
        echo "Please fill the Search field";
    }
    else {
        include_once("databaseService.php");
        $title = $_POST['search'];
        $books = searchBooks($_SESSION['email'], $title);

        echo '<table class="simpleTable">';

        echo "\n\n<tr>";
        echo '<th class="str"> Title  </th>';
        echo '<th class="str"> Author   </th>';
        echo '<th class="str"> Genres  </th>';
        echo '<th class="str"> Owners LName   </th>';
        echo '<th class="str"> Owners FName  </th>';
        echo '<th class="str"> Phone   </th>';
        echo '</tr class="str">';
        if ($books > 0) {
            foreach ($books as $book) {
                echo "\n<tr class=\"str\">";
                echo '<td class="str">' . $book->title . '</td>';
                echo '<td class="str">' . $book->author . '</td>';
                echo '<td class="str">' . $book->genres . '</td>';
                echo '<td class="str">' . $book->owner->lastName . '</td>';
                echo '<td class="str">' . $book->owner->firstName . '</td>';
                echo '<td class="str">' . $book->owner->phone . '</td>';
                echo '</tr class="str">';
            }
        }
    }
}

if (isset($_POST['sub'])) {
    if (empty($_POST['search'])) {
        echo "Please fill the Search field";
    }
    else {
        include_once("databaseService.php");
        $genres = $_POST['search'];
        $books = searchGBooks($_SESSION['email'], $genres);

        echo '<table class="simpleTable">';

        echo "\n\n<tr>";
        echo '<th class="str"> Title  </th>';
        echo '<th class="str"> Author   </th>';
        echo '<th class="str"> Genres  </th>';
        echo '<th class="str"> Owners LName   </th>';
        echo '<th class="str"> Owners FName  </th>';
        echo '<th class="str"> Phone   </th>';
        echo '</tr class="str">';
        if ($books > 0) {
            foreach ($books as $book) {
                echo "\n<tr class=\"str\">";
                echo '<td class="str">' . $book->title . '</td>';
                echo '<td class="str">' . $book->author . '</td>';
                echo '<td class="str">' . $book->genres . '</td>';
                echo '<td class="str">' . $book->owner->lastName . '</td>';
                echo '<td class="str">' . $book->owner->firstName . '</td>';
                echo '<td class="str">' . $book->owner->phone . '</td>';
                echo '</tr class="str">';
            }
        }
    }
}
?>