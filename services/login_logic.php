<?php
session_start(); // Starting Session
$error = ''; // Variable To Store Error Message

if (isset($_POST['submit'])) {
    if (empty($_POST['email']) || empty($_POST['password'])) {
        $error = "Username or Password is invalid";
    } else {
        // Define $username and $password
        $email = $_POST['email'];
        $password = $_POST['password'];
        // Establishing Connection with Server by passing server_name, user_id and password as a parameter
        include("database_connect.php");
        $connection = getConnection();
        // SQL query to fetch information of registerd users and finds user match.
        $query = sprintf("select * from user where password='%s' AND email='%s'",
            mysqli_real_escape_string($connection, $password),
            mysqli_real_escape_string($connection, $email)
        );
        $result = mysqli_query($connection, $query);
        $rows = mysqli_num_rows($result);

        if ($rows == 1) {
            $_SESSION['email'] = $email; // Initializing Session
            $row = mysqli_fetch_assoc($result);
            $_SESSION['username'] = $row['first_name'];
            header("location: mainPage.php"); // Redirecting To Other Page
            closeConnection($connection);
        } else {
            $error = "Username or Password is invalid";
        }
        closeConnection($connection); // Closing Connection
    }
}
?>