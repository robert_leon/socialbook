<?php
	//Creare conexiune
	function getConnection()
	{
		$connection = mysqli_connect("localhost", "root", "");
		mysqli_select_db($connection, "biblioteca");
		if (!$connection) {
			die("Connection failed: " . mysqli_connect_error());
		}

		return $connection;
	}

	function closeConnection($connection)
	{
		mysqli_close($connection);
	}
?>