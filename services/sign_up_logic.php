<?php
include('database_connect.php');
$connection = getConnection();
if (isset($_POST['Submit']))
{
    $fn = $_POST['firstname'];
    $ln = $_POST['lastname'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $password = $_POST['password1'];
    $city = $_POST['city'];
    $country = $_POST['country'];
    $query = sprintf(
        "INSERT INTO user(first_name, last_name, phone, email, password, city, country) VALUES('%s','%s','%s','%s','%s','%s','%s')",
        $fn, $ln, $phone, $email, $password, $city, $country);

    if(mysqli_query($connection, $query))
    {
        header("Location:login.php");
    }
    else
    {
        die (mysqli_error($connection));
    }
}