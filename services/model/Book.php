<?php
class Book
{
    var $bookId;
    var $title;
    var $author;
    var $genres;
    var $available;
    var $owner;
    var $requester;
}