<?php
include('database_connect.php');
//session_start();
$connection = getConnection();

if (isset($_POST['Submit']))
{
    $title = $_POST['title'];
    $author = $_POST['author'];
    $genres = $_POST['genres'];
    $owner_id = $_SESSION['email'];

    $query = sprintf(
        "INSERT INTO book(title, author, genres, owner_id ) VALUES('%s','%s','%s','%s')",
        $title, $author, $genres, $owner_id);

    if(mysqli_query($connection, $query))
    {
        header("Location:my_books.php");
    }
    else
    {
        die (mysqli_error($connection));
    }
}
?>