<html>
<head>
    <link rel="icon" type = "images/jpg" href = "images/logo-mic.jpg">
    <meta charset="UTF-8">
    <link href="css/shift.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<?php include("webComponents/loggedBar.php") ?>
<div class="row">
    <div class="col-sm-3">
        <div class="sidebar-nav">
            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span class="visible-xs navbar-brand">Sidebar menu</span>
                </div>
                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="my_books.php">My books <span class="badge">0</span></a></li>
                        <li><a href="my_requests.php">My requests <span class="badge">0</span></a></li>
                        <li><a href="add_book.php">Add Book</a></li>
                        <li class="active"><a href="search.php">Search books</a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div>
        <?php
        include("services/search_logic.php");
        ?>
        <h3>What is the title of the book you want?</h3>
        <form id="searchForm" action="" method="POST">
            <input name="search" type="text"  placeholder="Search book">
            <input type="submit" name="submit" value="Search"/>
        </form>
        <!-- Another search-->
        <h3>Search book by the genres you want:</h3>
        <form id="searchForm" action="" method="POST">
            <select name="search">
                <option value="Story">Story</option>
                <option value="Drama">Drama</option>
                <option value="Novel">Novel</option>
                <option value="Poetry">Poetry</option>
                <option value="Fiction">Fiction</option>
                <option value="Biography">Biography</option>
                <option value="History">History</option>
                <option value="Science">Science</option>
                <option value="Religious">Religious</option>
                <option value="Philosophy">Philosophy</option>
                <option value="Comic">Comic</option>
                <option value="Food">Food</option>
                <option value="Hobby">Hobby</option>
            </select>
            <!--<input name="search" type="text"  placeholder="Search genres">-->
            <input type="submit" name="sub" value="Search"/>
        </form>
    </div>
</div>
</body>
</html>
