<html>
<head>
	<link rel="icon" type = "images/jpg" href = "images/logo-mic.jpg">
	<meta charset="UTF-8">
	<link href="css/shift.css" rel="stylesheet">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
<?php include("webComponents/loggedBar.php") ?>
<div class="row">
	<div class="col-sm-3">
		<div class="sidebar-nav">
			<div class="navbar navbar-default" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<span class="visible-xs navbar-brand">Sidebar menu</span>
				</div>
				<div class="navbar-collapse collapse sidebar-navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="my_books.php">My books <span class="badge">0</span></a></li>
						<li><a href="my_requests.php">My requests <span class="badge">0</span></a></li>
						<li><a href="add_book.php">Add Book</a></li>
						<li><a href="search.php">Search books</a></li>

					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<div class="col-sm-9">
		<pre>
			<p style="text-indent: 30em;">Welcome!</p>
			<p style="text-indent: 5em;"> Here you can find you personal informations.</p>
			<p style="text-indent: 8em;">You can help our community get better by  <span><a href = add_book.php>adding a Book</a></span></p>
		</pre>
	</div>
</div>
</body>
</html>