-- MySQL dump 10.13  Distrib 5.6.16, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: biblioteca
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `book_id` int(255) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(255) NOT NULL,
  `title` varchar(40) NOT NULL,
  `author` varchar(40) NOT NULL,
  `genres` varchar(20) NOT NULL,
  `available` tinyint(1) NOT NULL,
  PRIMARY KEY (`book_id`),
  UNIQUE KEY `title` (`title`),
  KEY `owner_id` (`owner_id`),
  CONSTRAINT `book_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'iceangel23_94@yahoo.com','Scufita Rosie','Fratii Grimm','Story',0),(3,'daniel@yahoo.com','Scufita Neagra','Vasile Maciuca','Story',0),(4,'daniel@yahoo.com','Biologie marina','Nedelcu Marian','Science',0),(5,'iceangel23_94@yahoo.com','Gastronomie','Unknown','Food',0),(6,'gigi@gmail.com','Scufita Mov','Levi Molotov','Story',0),(7,'iceangel23_94@yahoo.com','Biblia','Unknown','Religious',0);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lending_request`
--

DROP TABLE IF EXISTS `lending_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lending_request` (
  `request_id` int(255) NOT NULL AUTO_INCREMENT,
  `b_id` int(255) NOT NULL,
  `description` varchar(200) NOT NULL,
  `status` varchar(5) NOT NULL,
  `lendee_id` varchar(255) NOT NULL,
  PRIMARY KEY (`request_id`),
  KEY `b_id` (`b_id`),
  KEY `lendee_id` (`lendee_id`),
  CONSTRAINT `lending_request_ibfk_1` FOREIGN KEY (`b_id`) REFERENCES `book` (`book_id`),
  CONSTRAINT `lending_request_ibfk_2` FOREIGN KEY (`lendee_id`) REFERENCES `user` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lending_request`
--

LOCK TABLES `lending_request` WRITE;
/*!40000 ALTER TABLE `lending_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `lending_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rating` (
  `rating_id` int(255) NOT NULL AUTO_INCREMENT,
  `req_id` int(255) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `rating_score` tinyint(5) NOT NULL,
  PRIMARY KEY (`rating_id`),
  KEY `req_id` (`req_id`),
  CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`req_id`) REFERENCES `lending_request` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating`
--

LOCK TABLES `rating` WRITE;
/*!40000 ALTER TABLE `rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(90) NOT NULL,
  `country` varchar(40) NOT NULL,
  `password` varchar(80) NOT NULL,
  `city` varchar(36) NOT NULL,
  `phone` varchar(16) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('Ana','Andronache','a@a.com','Romania','aaa','Brasov','0722654321'),('Andrei','Ciubotariu','andrei@yahoo.com','Romania','pass','Iasi','0754323210'),('Daniel','Leon','daniel@yahoo.com','Romania','pass','Timisoara','0735412645'),('Daniela','Vasilescu','daniela@yahoo.com','Romania','pass','Bucuresti','0734322103'),('Gigel','Munteanu','gigi@gmail.com','Romania','pass','Brasov','0773254321'),('Robert','Leon','iceangel23_94@yahoo.com','Romania','password','Iasi','0720550260');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'biblioteca'
--

--
-- Dumping routines for database 'biblioteca'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-11 18:16:24
